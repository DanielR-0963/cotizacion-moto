package com.daniel.moto.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Cotizacion")
public class Cotizacion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCotizacion;
    private Double montoTotal;
    private Double IVA;
    private Date fecha;
    private Double gastoAdministrativo;
    private Double costoAccesorioTotal;
    private Double enganche;

    @OneToOne
    @JoinColumn(name = "Vehiculo_idVehiculo")
    private Vehiculo vehiculo;

    @OneToOne
    @JoinColumn(name = "Cliente_idCliente")
    private Cliente cliente;

    public Integer getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(Integer idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public Double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Double getIVA() {
        return IVA;
    }

    public void setIVA(Double IVA) {
        this.IVA = IVA;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getGastoAdministrativo() {
        return gastoAdministrativo;
    }

    public void setGastoAdministrativo(Double gastoAdministrativo) {
        this.gastoAdministrativo = gastoAdministrativo;
    }

    public Double getCostoAccesorioTotal() {
        return costoAccesorioTotal;
    }

    public void setCostoAccesorioTotal(Double costoAccesorioTotal) {
        this.costoAccesorioTotal = costoAccesorioTotal;
    }

    public Double getEnganche() {
        return enganche;
    }

    public void setEnganche(Double enganche) {
        this.enganche = enganche;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
