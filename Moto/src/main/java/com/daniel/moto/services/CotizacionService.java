package com.daniel.moto.services;

import com.daniel.moto.entity.Cotizacion;

import java.util.List;

public interface CotizacionService {
    List<Cotizacion> findAll();
    void save (Cotizacion cotizacion);
    void update (Cotizacion cotizacion);
    void Delete (Cotizacion cotizacion);
    Cotizacion findByID(Integer idCotizacion);
}
