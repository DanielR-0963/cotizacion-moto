package com.daniel.moto.services;

import com.daniel.moto.entity.Cotizacion;
import com.daniel.moto.repository.CotizacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CotizacionServiceImpl implements CotizacionService{
    private CotizacionRepository repository;

    @Autowired
    public CotizacionServiceImpl(CotizacionRepository repository) {

        this.repository = repository;
    }

    @Override
    public List<Cotizacion> findAll() {

        List<Cotizacion> cotizacions = new ArrayList<>();

        for (Cotizacion iCotizacion: repository.findAll()) {

            SacarCostos(iCotizacion);

            cotizacions.add(iCotizacion);
        }

        return repository.findAll();
    }

    private void SacarCostos(Cotizacion iCotizacion) {
        iCotizacion.setIVA(iCotizacion.getVehiculo().getCosto() * 0.16);
        iCotizacion.setEnganche(iCotizacion.getVehiculo().getCosto() * 0.1);
        iCotizacion.setCostoAccesorioTotal(2000.0);
        iCotizacion.setGastoAdministrativo(150.0);
        iCotizacion.setMontoTotal(iCotizacion.getVehiculo().getCosto() + iCotizacion.getEnganche() +
                iCotizacion.getCostoAccesorioTotal() + iCotizacion.getIVA() + iCotizacion.getGastoAdministrativo());
    }

    @Override

    public void save(Cotizacion cotizacion) {
        repository.save(cotizacion);
    }

    @Override
    public void update(Cotizacion cotizacion) {
        repository.save(cotizacion);
    }

    @Override
    public void Delete(Cotizacion cotizacion) {
        repository.delete(cotizacion);
    }

    @Override
    public Cotizacion findByID(Integer idCotizacion) {
        Cotizacion cotizacion = repository.getById(idCotizacion);

        SacarCostos(cotizacion);

        return cotizacion;
    }
}
